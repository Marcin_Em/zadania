# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 16:20:44 2021

@author: Marcin Mokrzycki, 301985
"""
import random
import numpy as np
import matplotlib.pyplot as plt

mm = [100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 1000000]

def Monte(n):
    xkolo = 0
    x = []
    y = []
    x1 = []
    y1 = []
    x2 = []
    y2 = []  
    for i in range(n):
        x.append(random.uniform(-2, 8))
        y.append(random.uniform(-2, 8))
        if ((x[i]-3)**2 + (y[i]-3)**2) <= 25:
            x1.append(x[i])
            y1.append(y[i])
            xkolo = xkolo + 1 
        else: 
            x2.append(x[i])
            y2.append(y[i])
    x1 = np.array(x1)
    y1 = np.array(y1)
    x2 = np.array(x2)
    y2 = np.array(y2)
    return (4*(xkolo/n)), x1, y1, x2, y2

for i in range(len(mm)):
    wynik, xred, yred, xblue, yblue = Monte(mm[i])
    plt.figure()
    plt.title("Wykres dla n = %s " %(mm[i]))
    plt.plot(xred, yred, 'ro', label = 'Punkty w kole')
    plt.plot(xblue, yblue, 'bo',  label = 'Punkty poza kolem')
    plt.legend()
    plt.grid()
    plt.show()
    print ("Wartosc liczby pi dla n = %s , wynosi: %.5f" %(mm[i], wynik))