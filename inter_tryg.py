# -*- coding: utf-8 -*-
"""
Created on Sun Dec 20 21:47:00 2020

@author: Marcin Mokrzycki, 301985
"""

import numpy as np
import math as mh
import matplotlib.pyplot as plt

# f(x) = 7x - x^2

n = 3

x = np.arange(0,6,0.01)
x_ = []
y_ = []
X_ = []
y1 = []
y0 = []

def x_i(i, n):
    return ((2*i*mh.pi) / (2*n +1))

def f_x(m):
    return 7*m - m**2

def ret(x):
    return [1/mh.sqrt(2), mh.sin(x), mh.cos(x), mh.sin(2*x), mh.cos(2*x), mh.sin(3*x), mh.cos(3*x)]

def W(x):
    return A[0]/mh.sqrt(2) + A[1]*mh.sin(x) + A[2]*mh.cos(x) + A[3]*mh.sin(2*x) + A[4]*mh.cos(2*x) + A[5]*mh.sin(3*x) + A[6]*mh.cos(3*x)

for i in range(2*n+1):
    x_.append(x_i(i, n))
    y_.append(f_x(x_[i]))
    X_.append(ret(x_[i]))
        
X__ = np.array(X_)

X0 = (2/7) * X__.T

A = X0.dot(y_)

for i in range (len(x)):
    y1.append(W(x[i]))
    y0.append(f_x(x[i]))
    
print ("Znany wielomian: f(x) = 7x - x^2")
print ("Wielomian znaleziony w interpolacji: %.2f/sqrt(2) %.2f*sin(x) %.2f*cos(x) %.2f*sin(2x) %.2f*cos(2x) %.2f*sin(3x) %.2f*cos(3x)" %(A[0], A[1], A[2], A[3], A[4], A[5], A[6]))
    
plt.plot(np.array(x), np.array(y0), 'r', np.array(x), np.array(y1), 'b')
plt.scatter(x_, y_, color = 'g')
plt.grid(True)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Wykres")
plt.show