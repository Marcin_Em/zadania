# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 11:23:03 2020

@author: Marcin Mokrzycki, 301985
"""
import numpy as np
import math as ma

# f'(x) = (f(x+h) - f(x-h)) / 2h Pierwsze centralne przybliżenie różnicowe

def osX(alfa, beta):
    return 500 * ((ma.tan(beta)) / (ma.tan(beta) - (ma.tan(alfa))))

def osY(alfa, beta):
    return 500 * ((ma.tan(alfa) * ma.tan(beta)) / (ma.tan(beta) - ma.tan(alfa)))
    
t = np.array((9,10,11))                 # czas
A = np.array((54.80, 54.06, 53.34)) * (2*(ma.pi) / 360)    # alfa
B = np.array((65.59, 64.59, 63.62)) * (2*(ma.pi) / 360)    # beta

osXprim = ((osX(A[2], B[2])) - (osX(A[0], B[0]))) / 2   # x'
osYprim = ((osY(A[2], B[2])) - (osY(A[0], B[0]))) / 2   # y'

V = ma.sqrt((osXprim**2 + osYprim**2))  # predkosc

kat = ma.atan(osYprim / osXprim) * (180 / ma.pi)       # kat wznoszenia

print ("Predkosc samolotu w wynosi %.4f m/s , natomiast kat wznoszenia wynosi %.4f stopnia , w t = 10 s. " % (V, kat))