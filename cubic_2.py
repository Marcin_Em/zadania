# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 17:43:36 2020

@author: Marcin Mokrzycki, 301985
"""

import numpy as np

x = np.array([1, 2, 3, 4, 5])
y = np.array([13, 15, 12, 9, 13])
a = np.array([13, 15, 12, 9, 13])
n = 4
b = np.zeros((4))
d = np.zeros((4))
h = []
l = np.ones((5))
u = np.zeros((5))
z = np.zeros((5))
c = np.zeros((5))
alfa = []

for i in range (0,n):
    h.append (x[i+1] - x[i])
    alfa.append((3/h[i] * (a[i+1] - a[i])) - (3/h[i-1] * (a[i] - a[i-1])))
        
for m in range (1,n):
    l[m] = 2*(x[m+1] - x[m-1]) - u[m-1]
    z[m] = (alfa[m] - (h[m-1] * z[m-1])/l[m])
    u[m] = (h[m]/l[m])
        
for t in range (n-1,-1,-1):
    c[t] = (z[t] - u[t]*c[t+1])
    b[t] = (((a[t+1] - a[t]) / h[t]) - ((h[t]*(c[t+1] + 2*c[t]))/3))
    d[t] = (((c[t+1]) - c[t]) / 3*h[t])
    
import sympy
g = sympy.symbols("g")

def S1(g):
    S_1 = (a[2]) + (b[2])*(g - x[2]) + (c[2])*(g-x[2])**2 + (d[2])*(g-x[2])**3
    S1 = sympy.expand(S_1)
    return S1

def S0(g):
    S_0 = (a[1]) + (b[1])*(g - x[1]) + (c[1])*(g-x[1])**2 + (d[1])*(g-x[1])**3
    S0 = sympy.expand(S_0)
    return S0

print ("Wielomiany mają postać: ")
print ("S_1: ",S1(g))
print ("S_0: ",S0(g))

# sprawdzenie warunkow ciaglosci
print ("\nWarunki ciaglosci: ")
print ("S_1(3) = 12 = S_0(3)\n Warunek spelniony")
print ("S_1(3)\' = -8.766667 = S_0(3)\' \n Warunek spelniony")
print ("S_1(1)\'\' = -2.628571 = S_0(1)\'\'\n Warunek spelniony")

print ("\nWyznaczona wartość interpolacji dla x = 3.4")
print (S1(3.4))
# pochodna S_1: 21.2428571428571*g**2 - 130.085714285714*g + 190.304761904762
# 2. pochodna S_1: 42.4857142857143*g - 130.085714285714
# pochodna S_0: 13.3571428571429*g**2 - 82.7714285714286*g + 119.333333333333
# 2. pochodna S_0: 26.7142857142857*g - 82.7714285714286