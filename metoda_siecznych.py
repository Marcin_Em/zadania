# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 12:02:02 2020

@author: Marcin Mokrzycki, 301985
"""

from math import *

def f(x):
    global name
    return eval(str(name))

def wpisanie_wartosci_A():
    while True:
        try:
            a = float(input("Wprowadź wartość dolnej granicy przedziału \n")) 
        except ValueError:
            print("\nWprowadzono niepoprawne dane")
            continue
        else:
            break      
    return a

def wpisanie_wartosci_B():
    while True:
        try:
            b = float(input("Wprowadź wartość gornej granicy przedziału \n")) 
        except ValueError:
            print("\nWprowadzono niepoprawne dane")
            continue
        else:
            break      
    return b


name = input(u"Wpisz funkcję z której chcesz poznac miejsce zerowe w danym zakresie \n") 

a = wpisanie_wartosci_A()
b = wpisanie_wartosci_B()
E = 0.00000000000001
i = 64


fa, fb = f(a), f(b)
x0 = 0


if i < 0:
    print ("Przekroczono limit obioegów")
else:
    while i > 0:
        if ((abs(a-b))<E):
            print("Miejsce zerowe funkcji")
            print (x0)
            break
        else:
            if ((abs(fa-fb))<E):
                print ("Źle dobrane punkty startowe")
                break
            else:
                x0 = a - ((fa)*((a-b)/(fa-fb)))
                f0 = f(x0)
                if (abs(f0)<E):
                    print("Miejsce zerowe funkcji")
                    print (x0)
                    break
                else:
                    b, fb, = a, fa
                    a, fa = x0, f0
                    i -= 1  
