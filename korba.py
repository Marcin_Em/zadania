# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 13:41:17 2021

@author: Marcin Mokrzycki, 301985
"""
import math
import numpy as np
import pandas as pd

def funkcja(x):
    return 0.09*(math.cos(x) + (math.sqrt((((2.5)**2)) - ((math.sin(x))**2))))

def uzywany_wzor(x, h):
    return ((funkcja(x+2*h))-(2*(funkcja(x+h)))+(funkcja(x)))/(h**2)

katt = []
a = []
wiersze = []
for i in range(0,181,5):
    katt.append(i * (2*(math.pi) / 360))
kat = np.array(katt) 
for i in range(len(kat)):
    a.append(uzywany_wzor(kat[i], (5*((2*(math.pi))/360))))
for j in range(len(kat)):               # obliczanie wyniku
    kat[j] = kat[j] * (180 / math.pi)
for x in range(len(kat)):                 # iteracja do pandas
    wiersze.append(x)
a = np.array(a)
tabs = np.vstack((kat, a))
tabs = np.transpose(tabs)
tabela=pd.DataFrame(tabs, [wiersze], [u'Kąt [stopnie]', 'Przyspieszenie'])
print (tabela)