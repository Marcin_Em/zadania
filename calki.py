# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 12:26:51 2020

@author: Marcin Mokrzycki, 301985
"""
import math
import numpy as np
import pandas as pd

def f(x):
    return 2*math.sin(x)

def prostokat():
    for yy in range(4):
        dx = ((xk - xp)/X[yy])
        for i in range(X[yy]):
            xi = xp + ((i/X[yy])*(xk - xp))
            
            fxi = f(xi)
            suma[yy] = suma[yy] + fxi
        suma[yy] = dx * suma[yy]
    return suma

def trapez():
    for yy in range(4):
        dx = ((xk - xp)/X[yy])
        for i in range(X[yy]):
            xi = xp + ((i/X[yy])*(xk - xp))
            fxi = f(xi)
            suma1[yy] = suma1[yy] + fxi
        suma1[yy] = (suma1[yy] + ((f(xp))+f(xk))/2)*dx
    return suma1

def Simpson():
    for yy in range(4):
        dx = ((xk - xp)/X[yy])
        for i in range(X[yy]):
            x = xp + ((i/X[yy])*(xk - xp))
            sumat[yy] = sumat[yy] + (f(x - (dx/2)))
            if i < X[yy]:
                suma2[yy] = suma2[yy] + f(x)
        suma2[yy] = dx/6 * ((f(xp)+f(xk)+2*suma2[yy]+4*sumat[yy]))
    return suma2

# dane             
XX = [2,8,16,32]    # n
X = np.array(XX)
suma = np.zeros(4)
suma1 = np.zeros(4)
suma2 = np.zeros(4)
sumat = np.zeros(4)
xp = 0
xk = 4

wynik = np.vstack((prostokat(), trapez(), Simpson()))
# odpowiedz
print ("Obliczana jest ca�ka z funkcji f(x) = 2sin(x) , na przedziale [0-4]")
print ("Wynik obliczony analitycznie")
print (-2*math.cos(4)+2)

tabela=pd.DataFrame(wynik, ['Prostokaty', 'Trapezy', 'Simpson'], ['n=2', 'n=8', 'n=16', 'n=32'])
print (tabela)