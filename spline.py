# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 17:43:36 2020

@author: Marcin Mokrzycki, 301985
"""

import numpy as np

x = np.array([0, 1, 2])
y = np.array([0, 2, 1])
a = np.array([0, 2, 1])

b = np.array([0,0])
d = np.array([0,0])
h = []
l = np.array([1,1,1,1])
u = [0]
z = [0]
c = np.array([0,0,0,0])
alfa = []

for i in range (0,2):
    h.append (x[i+1] - x[i])

alfa.append((3/h[1] * (a[2] - a[1])) - (3/h[0] * (a[1] - a[0])))

for i in range (1,2):
    l[i] = (2*(x[i+1] - x[i-1]) - h[i]*u[i-1])
    u.append(h[i] / l[i])
    z.append(alfa[0] - (h[i-1] * z[i-1])/l[i])
    
    
for t in range (1,-1,-1):
    c[t] = (z[t] - u[t]*c[t+1])
    b[t] = (((a[t+1] - a[t]) / h[t]) - ((h[t]*(c[t+1] + 2*c[t]))/3))
    d[t] = (((c[t+1]) - c[t]) / 3*h[t])

import sympy

g, dd = sympy.symbols("g, dd")

def S1(g):
    S_1 = a[1] + b[1]*(g - x[1]) + c[1]*(g-x[1])**2 + d[1]*(g-x[1])**3
    S1 = sympy.expand(S_1)
    return S1

def S0(g):
    S_0 = a[0] + b[0]*(g - x[0]) + c[0]*(g-x[0])**2 + d[0]*(g-x[0])**3
    S0 = sympy.expand(S_0)
    return S0


print ("Wielomiany mają postać: ")
print ("S_1: ",S1(g))
print ("S_0: ",S0(g))

# sprawdzenie warunkow ciaglosci


print ("\n\nS_1(1) = 2 = S_0(1)\n Warunek spelniony")
print ("S_1(1)\' = 5,  S_0(1)\' = -4 \n Warunek niespelniony")
print ("S_1(1)\'\' = -18 = S_0(1)\'\' \n Warunek spelniony")
print ("S_0(0)\'\' = 0 = S_1(2)\'\' \n Warunek spelniony")

import matplotlib.pyplot as plt
x1 = np.linspace(0, 2, 200)

plt.plot(x1, 9*x1**2 - 36*x1 + 32, label = 'S1\'')
plt.plot(x1, -9*x1**2 + 5, label = 'S0\'')
plt.title("Wykres wielomianow gdzie nie ma warunku ciaglosci")
plt.grid(True)
plt.legend()
plt.show()