# -*- coding: utf-8 -*-
"""
Created on Mon Dec 14 19:29:35 2020

@author: Marcin Mokrzycki, 301985
"""

import numpy as np

x1, x2, x3, y1, y2, y3 = 0, 1, 2, 0, 2, 1

def macierz2x2():
    wyznacznik2x2 = A1[0][0]*A1[1][1] - A1[0][1]*A1[1][0]
    return wyznacznik2x2

# 0 = a * 0**2 + b * 0**1 + c 
# 2 = a * 1**2 + b * 1**1 + c
# 1 = a * 2**2 + b * 2**1 + c

# A^(-1) * B = [a, b, c]^(-1) 

A = np.array([[0, 0, 1], [1, 1, 1], [4, 2, 1]])
B = np.array([[0], [2], [1]])

# obliczania macierzy odwrotnej A
C = np.ones((3,3))
# obliczanie wyznacznika macierzy 3x3
for i in range(1):
    wyznacznik = 0
    for j in range(3):
        a1 = np.delete(A,i,0)
        A1 = np.delete(a1,j,1)
        wynik = ((-1)**((i+1)+(j+1))) * A[0][j] * macierz2x2()
        wyznacznik = wyznacznik + wynik
    
if wyznacznik != 0:
    for i in range(3):
        for j in range(3):
            a1 = np.delete(A,i,0)
            A1 = np.delete(a1,j,1)
            ad = macierz2x2()
            C[i][j] = (((-1)**((i+1)+(j+1)))*ad)
    trans = C.T        
    A_1 = (1/wyznacznik) * trans
          
else:
    print ("Nie można obliczyć macierzy odwrotnej, ponieważ wyznacznik wynosi 0")

X = A_1 @ B
print ("Wielomian ma postac: %.3f * x^2 + %.3f * x + %.3f" % (X[0][0], X[1][0], X[2][0]) )