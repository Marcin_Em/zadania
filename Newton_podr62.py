# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 12:12:11 2020

@author: Marcin
"""

import sympy
from math import *
import numpy as np
import datetime

U = np.zeros((3,1))
F = np.zeros((3,1))
X = np.zeros((3,1))


for i in range(3):
    print ("Wprowadz x ",i)
    X[i][0] = float(input())
    
start = datetime.datetime.now()

x, y, z = sympy.symbols('x y z')
a = 3
n = a-1
name = x**2 + y**2 + z**2 - 3
name1 = 2*x*y + 3*x*z - y*z - 4
name2 = 2*x - 3*y + z

def f1(x,y,z):
    global name
    return eval(str(name))

def f2(x,y,z):
    global name1
    return eval(str(name1))

def f3(x,y,z):
    global name2
    return eval(str(name2))

def fW(x,y,z):
    global arr
    return eval(str(arr))
  
def obl():
    
    for s in range(n):
        for x in range(s+1,a):          ## właściwa petla
            for y in range(s+1,a+1):
                C[x][y] = kekw(x,y,s)
    for x in range(s+1,a):        ## ustawienie 0 w odpowiednich miejscach
        for y in range(s+1):
            C[x][y] = kekw(x,y,s)
    F[n]=C[n][n+1]/C[n][n]
    for i in range(n-1,-1,-1):
        suma=0
        for s in range(i+1,n+1):
            suma=suma + (C[i][s] * F[s])
        F[i]=(C[i,n+1] - suma)/C[i][i]
    return F

def kekw(x,y,s):
    cyfra = (C[x][y])-((((C[x][s])*(C[s][y]))/(C[s][s])))
    return cyfra
  
itera = 23
E = 0.00001

arr=[]
arr.append (sympy.diff(f1(x,y,z), x))
arr.append (sympy.diff(f1(x,y,z), y))
arr.append (sympy.diff(f1(x,y,z), z))
arr.append (sympy.diff(f2(x,y,z), x))
arr.append (sympy.diff(f2(x,y,z), y))
arr.append (sympy.diff(f2(x,y,z), z))
arr.append (sympy.diff(f3(x,y,z), x))
arr.append (sympy.diff(f3(x,y,z), y))
arr.append (sympy.diff(f3(x,y,z), z))

k = 0
while k <= itera:

    for i in range(3):
        U[i][0] = X[i][0]
        
    A = fW(X[0][0],X[1][0],X[2][0])
    M = np.array(A)
    W = M.reshape(3,3)
    
    F[0] = -(f1(X[0][0],X[1][0],X[2][0]))
    F[1] = -(f2(X[0][0],X[1][0],X[2][0]))
    F[2] = -(f3(X[0][0],X[1][0],X[2][0]))
    
    C = np.hstack((W,F))
    dX = obl()
    X[0] = X[0] + dX[0]
    X[1] = X[1] + dX[1]
    X[2] = X[2] + dX[2]
    
    licz = 0
    for i in range(3):
        if abs(X[i] - U[i]) <= E:
            licz = licz + 1
    if licz == n:
        for i in range (3):
            print (X[i])
            break
    k = k+1
    
print (X)
duration = datetime.datetime.now() - start
print (duration)