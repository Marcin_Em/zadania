# -*- coding: utf-8 -*-
"""
Created on Mon Jan  4 13:51:22 2021

@author: Marcin Mokrzycki, 301985
"""

import math
import numpy as np

F = np.array([0, 37, 71, 104, 134, 161, 185, 207, 225, 239, 250])
m, xk, xp = 0.075, 0.5, 0
dx = ((xk - xp)/(len(F)-1))
for x in range(1,len(F)):
    ad = ((sum(F,-F[x])) + (((F[x-1]+F[x])/2))) * dx

v = math.sqrt((2*ad)/m)
print ("Predkosc strzaly wynosi" ,v, "m/s")