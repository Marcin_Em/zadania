# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 12:45:20 2020

@author: Marcin Mokrzycki, 301985
"""
import sympy
from math import *

def f(x):
    global name
    return eval(str(name))

def fprim(x):
    global poch
    return eval(str(poch))

def wpisanie_wartosci_A():
    while True:
        try:
            a = input("Wprowadź funkcje \n")
        except ValueError:
            print("\nWprowadzono niepoprawne dane")
            continue
        else:
            break      
    return a

def wpisanie_wartosci_x0():
    while True:
        try:
            a = float(input("Wprowadź wartość x0 \n")) 
        except ValueError:
            print("\nWprowadzono niepoprawne dane")
            continue
        else:
            break      
    return a

x = sympy.Symbol('x')
name = wpisanie_wartosci_A()
i = 64
x0 = wpisanie_wartosci_x0()
x1 = x0 - 1
E = 0.000000001
f0 = f(x0)
poch = sympy.diff(name)

while i > 0:
    if i > 0:
        if abs(x1 - x0) > E:
            if abs(f0) > E:
                f1 = fprim(x0)
                if abs(f1) < E:
                    print ("Miejsce zerowe")
                    break
                else:
                    x1 = x0
                    x0 = x0 - (f0 / f1)
                    f0 = f(x0)
                    i = i - 1
            else:
                print ("Miejsce zerowe ")
                print (x0)
                break
        else:
            print ("pierw")
            print (x0)
            break
    else:
        print ("Przekroczony limit obiegów")
        break