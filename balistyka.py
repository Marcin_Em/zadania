# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 13:45:55 2021

@author: Marcin Mokrzycki, 301985
"""

import numpy as np
import math
import pandas as pd
import matplotlib.pyplot as plt

### bez oporow powietrza
def Vxprim(a):                      # Vxprim
    return a
def xprim(Vx):                      # xprim
    return Vx
def Vyprim(a):                      # Vyprim
    return -9.81 #przyspieszenie ziemskie
def yprim(Vy):                      # y prim
    return Vy


    # Metoda Eulera
def Eulera(h):
    Vx = [500 * math.cos(2*math.pi / 360 * 30)]     # predkosc x
    Vy = [500 * math.sin(2*math.pi / 360 * 30)]     # predkosc y
    y = [0]
    x = [0]
    x1 = [0]
    
    for i in range(98080):
        x1.append(x1[i] + h) 
        Vx.append(Vx[i] + Vxprim(0) * h) 
        Vy.append(Vy[i] + Vyprim(x1[i]) * h) 
        x.append((x[i] + xprim(Vx[i]) * h))
        y.append(y[i] + yprim(Vy[i]) * h)
        if y[i] < 0:
            break
    return y, x, Vx, Vy, x1

y, x, Vx, Vy, x1 = Eulera(0.1)



# metoda Rungego-Kutty
def Kutt(h):
    
    y_a = [500 * math.cos(2*math.pi / 360 * 30)]        # tablice do kutty
    K1a = [0]
    K2a = [0]
    K3a = [0]
    K4a = [0]
    y_b = [500 * math.sin(2*math.pi / 360 * 30)]
    K1b = [0]
    K2b = [0]
    K3b = [0]
    K4b = [0]
    y_c = [0]
    K1c = [0]
    K2c = [0]
    K3c = [0]
    K4c = [0]
    y_d = [0]
    K1d = [0]
    K2d = [0]
    K3d = [0]
    K4d = [0]
    
    for i in range (983):       # podstawianie odpowiednich wartosci
    
        y_a.append(0)
        K1a.append(0)
        K2a.append(0)
        K3a.append(0)
        K4a.append(0)
        K1a[i] = h * Vxprim(0)
        K2a[i] = h * Vxprim(K1a[i]/2 + 0)
        K3a[i] = h * Vxprim(0 + K2a[i]/2)
        K4a[i] = h * Vxprim(0 + K3a[i])
        y_a[i+1] = y_a[i] + ((1/6)*(K1a[i] + 2*K2a[i] + 2*K3a[i] + K4a[i]))
        y_b.append(0)
        K1b.append(0)
        K2b.append(0)
        K3b.append(0)
        K4b.append(0)
        K1b[i] = h * Vyprim(y_b[i])
        K2b[i] = h * Vyprim(K1b[i]/2 + y_b[i])
        K3b[i] = h * Vyprim(y_b[i] + K2b[i]/2)
        K4b[i] = h * Vyprim(y_b[i] + K3b[i])
        y_b[i+1] = y_b[i] + ((1/6)*(K1b[i] + 2*K2b[i] + 2*K3b[i] + K4b[i]))
        y_c.append(0)
        K1c.append(0)
        K2c.append(0)
        K3c.append(0)
        K4c.append(0)
        K1c[i] = h * yprim(y_b[i])
        K2c[i] = h * yprim(K1b[i]/2 + y_b[i])
        K3c[i] = h * yprim(y_b[i] + K2b[i]/2)
        K4c[i] = h * yprim(y_b[i] + K3b[i])
        y_c[i+1] = y_c[i] + ((1/6)*(K1c[i] + 2*K2c[i] + 2*K3c[i] + K4c[i]))
        y_d.append(0)
        K1d.append(0)
        K2d.append(0)
        K3d.append(0)
        K4d.append(0)
        K1d[i] = h * xprim(y_a[i])
        K2d[i] = h * xprim(K1d[i]/2 + y_a[i])
        K3d[i] = h * xprim(y_a[i] + K2d[i]/2)
        K4d[i] = h * xprim(y_a[i] + K3d[i])
        y_d[i+1] = y_d[i] + ((1/6)*(K1d[i] + 2*K2d[i] + 2*K3d[i] + K4d[i]))
        if y_c[i+1] < 0:
            break
    return y_a, y_b, y_c, y_d

Vx, Vy, y1, x1 = Kutt(0.1)

plt.figure()
plt.plot(x, y, label = 'Metoda Eulera')
plt.plot(x1, y1, label = 'Metoda Kutty')
plt.title("Wykres bez oporu powietrza")
plt.grid(True)
plt.legend()
plt.show()

### z oporami powietrza

def Vxprimo(x, y):                      # Vxprim
    return -0.03 * x * math.sqrt(math.sqrt(x**2 + y**2))
def Vyprimo(x, y):                      # Vyprim
    return -0.03 * y * math.sqrt(math.sqrt(x**2 + y**2)) - 9.81 #przyspieszenie ziemskie
def xprimo(Vx):                      # xprim
    return Vx
def yprimo(Vy):                      # y prim
    return Vy


def Eulerao(h):
    Vx = [500 * math.cos(2*math.pi / 360 * 30)]     # predkosc x
    Vy = [500 * math.sin(2*math.pi / 360 * 30)]     # predkosc y
    y = [0]
    x = [0]
    x1 = [0]
    
    for i in range(983):
        x1.append(x1[i] + h) 
        Vx.append(Vx[i] +  h* Vxprimo((Vx[i]), (Vy[i])) ) 
        Vy.append(Vy[i] +  h* Vyprimo((Vx[i]), (Vy[i])) ) 
        x.append((x[i] + xprimo(Vx[i]) * h))
        y.append(y[i] + yprimo(Vy[i]) * h)
        if y[i] < 0:
            break
    return y, x, Vx, Vy, x1

yeo, xeo, Vexo, Veyo, x1o = Eulerao(0.1)

# metoda Rungego-Kutty
def Kutto(h):
    
    y_a = [500 * math.cos(2*math.pi / 360 * 30)]        # tablice do kutty
    K1a = [0]
    K2a = [0]
    K3a = [0]
    K4a = [0]
    y_b = [500 * math.sin(2*math.pi / 360 * 30)]
    K1b = [0]
    K2b = [0]
    K3b = [0]
    K4b = [0]
    y_c = [0]
    K1c = [0]
    K2c = [0]
    K3c = [0]
    K4c = [0]
    y_d = [0]
    K1d = [0]
    K2d = [0]
    K3d = [0]
    K4d = [0]
    
    for i in range (983):       # podstawianie odpowiednich wartosci
        y_a.append(0)
        K1a.append(0)
        K2a.append(0)
        K3a.append(0)
        K4a.append(0)
        K1a[i] = h * Vxprimo(y_a[i], y_b[i])
        K2a[i] = h * Vxprimo(y_a[i] + h/2 , K1a[i]/2 + y_b[i])
        K3a[i] = h * Vxprimo(y_a[i] + h/2 , y_b[i] + K2a[i]/2)
        K4a[i] = h * Vxprimo(y_a[i] + h , y_b[i] + K3a[i])
        y_a[i+1] = y_a[i] + ((1/6)*(K1a[i] + 2*K2a[i] + 2*K3a[i] + K4a[i]))
        y_b.append(0)
        K1b.append(0)
        K2b.append(0)
        K3b.append(0)
        K4b.append(0)
        K1b[i] = h * Vyprimo(y_a[i], y_b[i])
        K2b[i] = h * Vyprimo(y_a[i] + h/2 , K1b[i]/2 + y_b[i])
        K3b[i] = h * Vyprimo(y_a[i] + h/2 , y_b[i] + K2b[i]/2)
        K4b[i] = h * Vyprimo(y_a[i] + h , y_b[i] + K3b[i])
        y_b[i+1] = y_b[i] + ((1/6)*(K1b[i] + 2*K2b[i] + 2*K3b[i] + K4b[i]))
        y_c.append(0)
        K1c.append(0)
        K2c.append(0)
        K3c.append(0)
        K4c.append(0)
        K1c[i] = h * yprimo(y_b[i])
        K2c[i] = h * yprimo(K1c[i]/2 + y_b[i])
        K3c[i] = h * yprimo(y_b[i] + K2c[i]/2)
        K4c[i] = h * yprimo(y_b[i] + K3c[i])
        y_c[i+1] = y_c[i] + ((1/6)*(K1c[i] + 2*K2c[i] + 2*K3c[i] + K4c[i]))
        y_d.append(0)
        K1d.append(0)
        K2d.append(0)
        K3d.append(0)
        K4d.append(0)
        K1d[i] = h * xprimo(y_a[i])
        K2d[i] = h * xprimo(K1d[i]/2 + y_a[i])
        K3d[i] = h * xprimo(y_a[i] + K2d[i]/2)
        K4d[i] = h * xprimo(y_a[i] + K3d[i])
        y_d[i+1] = y_d[i] + ((1/6)*(K1d[i] + 2*K2d[i] + 2*K3d[i] + K4d[i]))
        if y_c[i+1] < 0:
            break
    return y_a, y_b, y_c, y_d

Vxo, Vyo, y1o, x1o = Kutto(0.1)



plt.figure()
plt.plot(xeo, yeo, label = 'Metoda Eulera')
plt.plot(x1o, y1o, label = 'Metoda Kutty')
plt.title("Wykres z oporem powietrza")
plt.grid(True)
plt.legend()
plt.show()