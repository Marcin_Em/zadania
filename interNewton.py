# -*- coding: utf-8 -*-
"""
Created on Sun Dec 13 13:25:13 2020

@author: Marcin Mokrzycki,  301985
"""
import numpy as np
import sympy as sy

xData = np.array((-3,2,-1,3,1))
yData = np.array((0,5,-4,-12,0))
a=yData
n=len(xData)
for k in range (1,n):
    a[k:n]=(a[k:n]-a[k-1])/(xData[k:n] - xData[k-1])

print ("Tablica wspolczynnikow:")
print (a)