# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 16:20:44 2021

@author: Marcin Mokrzycki, 301985
"""
import random
import math as ma
import numpy as np
import matplotlib.pyplot as plt

mm = [100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 1000000]

def Monte(n):
    R = 960
    r = 65
    xkolo = 0
    x = []
    y = []
    z = []
    z1 = []
    x1 = []
    y1 = []
    z2 = []
    x2 = []
    y2 = []  
    for i in range(n):
        x.append(random.uniform(-960, 960))
        y.append(random.uniform(-960, 960))
        z.append(random.uniform(-65,65))
        if (((ma.sqrt((x[i]**2)+(y[i]**2)))-R)**2) + z[i]**2 <= r**2:
            x1.append(x[i])
            y1.append(y[i])
            z1.append(z[i])
            xkolo = xkolo + 1 
        else: 
            x2.append(x[i])
            y2.append(y[i])
            z2.append(y[i])
    x1 = np.array(x1)
    y1 = np.array(y1)
    z1 = np.array(z1)
    x2 = np.array(x2)
    y2 = np.array(y2)
    z2 = np.array(z2)
    return (((65*2 * 960*2 * 960*2)/n)*xkolo), x1, y1, z1, x2, y2, z2, x, y, z

for i in range(len(mm)):
    wynik, xred, yred, zred, xblue, yblue, zblue, x, y, z = Monte(mm[i])
    fig = plt.figure()
    ax = plt.axes(projection="3d")
    plt.title("Wykres dla n = %s " %(mm[i]))
    ax.scatter3D(xred, yred, zred, c=zred, cmap='jet')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    plt.grid()
    plt.show()
    print ("Objetosc dla n = %s , wynosi: %.5f" %(mm[i], wynik))
print ("Objetosc obliczona analitycznie: %.9f" %(2 * 3.14**2 * 895 * 65**2))