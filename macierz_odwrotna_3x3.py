# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 20:57:48 2020

@author: Marcin
"""
import numpy as np

def wpisanie_wartosci_A(x,y):
    while True:
        try:
            b = float(input("Wprowadź wartość dla wiersza %d kolumna %d macierzy A \n" % (x,y)))
        except ValueError:
            print("\nWprowadzono niepoprawne dane")
            continue
        else:
            break      
    return b

def macierz2x2():
    wyznacznik2x2 = A1[0][0]*A1[1][1] - A1[0][1]*A1[1][0]
    return wyznacznik2x2

A = np.zeros((3,3))
for x in range(3):
    for y in range(3):
        xx = wpisanie_wartosci_A(x,y)
        A[x][y] = xx        

C = np.ones((3,3))
print ("Podana macierz A: ")
print (A)

# obliczanie wyznacznika macierzy 3x3
for i in range(1):
    wyznacznik = 0
    for j in range(3):
        a1 = np.delete(A,i,0)
        A1 = np.delete(a1,j,1)
        wynik = ((-1)**((i+1)+(j+1))) * A[0][j] * macierz2x2()
        wyznacznik = wyznacznik + wynik
print ("Wyznacznik: ")
print (wyznacznik)

if wyznacznik != 0:
    for i in range(3):
        for j in range(3):
            a1 = np.delete(A,i,0)
            A1 = np.delete(a1,j,1)
            ad = macierz2x2()
            C[i][j] = (((-1)**((i+1)+(j+1)))*ad)
    trans = C.T        
    A_1 = (1/wyznacznik) * trans
    print ("Macierz odwrotna ma postać: ")
    print (A_1)
else:
    print ("Nie można obliczyć macierzy odwrotnej, ponieważ wyznacznik wynosi 0")