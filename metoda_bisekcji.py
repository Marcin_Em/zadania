# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 17:50:24 2020

@author: Marcin Mokrzycki, 301985
"""
from math import *

name = input(u"Wpisz funkcję z której chcesz poznac miejsce zerowe w danym zakresie \n")   

def f(x):
    global name
    return eval(str(name))

def wpisanie_wartosci_A():
    while True:
        try:
            a = float(input("Wprowadź wartość dolnej granicy przedziału \n")) 
        except ValueError:
            print("\nWprowadzono niepoprawne dane")
            continue
        else:
            break      
    return a

def wpisanie_wartosci_B():
    while True:
        try:
            b = float(input("Wprowadź wartość gornej granicy przedziału \n")) 
        except ValueError:
            print("\nWprowadzono niepoprawne dane")
            continue
        else:
            break      
    return b

def wpisanie_wartosci_E():
    while True:
        try:
            E = float(input("Wprowadź wartość dokładnosci przy wykonywaniu obliczen \n")) 
        except ValueError:
            print("\nWprowadzono niepoprawne dane")
            continue
        else:
            break      
    return E

a = wpisanie_wartosci_A()
b = wpisanie_wartosci_B()
E = wpisanie_wartosci_E()



fa, fb = f(a), f(b)
if fa * fb > 0: 
    print ("Funkcja nie spelnia zalozen \n")
else:
    while abs(a - b) > E:
        x0 = (a + b) / 2
        f0 = f(x0)
        if abs(f0) < E: 
            break 
        if fa * f0 < 0:
            b = x0
        else: 
            a = x0
            fa = f0
    print ("Obliczone miejsce zerowe")
    print (x0)