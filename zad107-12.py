# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 12:32:27 2020

@author: Marcin
"""
import numpy as np
wys = np.array((0, 3, 6))   #[km]
h = np.array((1.225, 0.905, 0.652))

a = (h[0] / ((wys[0] - wys[1]) * (wys[0] - wys[2]))) + \
(h[1] / ((wys[1] - wys[0]) * (wys[1] - wys[2]))) + \
(h[2] / ((wys[2] - wys[0]) * (wys[2] - wys[1]))) 

b = -((h[0] * (wys[1] + wys[2])) / ((wys[0] - wys[1]) * (wys[0] - wys[2]))) - \
((h[1] * (wys[0] + wys[2])) / ((wys[1] - wys[0]) * (wys[1] - wys[2]))) - \
((h[2] * (wys[0] + wys[1])) / ((wys[2] - wys[0]) * (wys[2] - wys[1])))

c = ((h[0] * wys[1] * wys[2]) / ((wys[0] - wys[1]) * (wys[0] - wys[2]))) + \
((h[1] * wys[0] * wys[2]) / ((wys[1] - wys[0]) * (wys[1] - wys[2]))) + \
((h[2] * wys[0] * wys[1]) / ((wys[2] - wys[0]) * (wys[2] - wys[1])))

print ("%fx^2 %fx %f" % (a, b, c))