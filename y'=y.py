# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 13:45:55 2021

@author: Marcin Mokrzycki, 301985
"""

import numpy as np
import math
import pandas as pd
import matplotlib.pyplot as plt

def y_ogolny(u):
    return u

def y_scisly(u):
    return math.exp(u)

print ("Rozwiąż równanie różniczkowe y'= y z warunkiem początkowym y(0)=1 metodą Eulera i metodą punktu pośredniego w przedziale x = [0,10].")

    # Metoda Eulera
def Eulera(h):
    
    y = [1]
    x = [0]
        
    for i in range(int(10/h)+1):
        x.append(x[i] + h)
        y[i] = y_scisly(x[i])
        y.append(0)
    del y[-1]
    del x[-1]
    return y, x


# metoda Rungego-Kutty
def Kutt(h):
    
    y_ = [1]
    K1 = [0]
    K2 = [0]
    K3 = [0]
    K4 = [0]
        
    for i in range (int(10/h)):
        y_.append(0)
        K1.append(0)
        K2.append(0)
        K3.append(0)
        K4.append(0)
        K1[i] = h * y_ogolny(y_[i])
        K2[i] = h * y_ogolny(K1[i]/2 + y_[i])
        K3[i] = h * y_ogolny(y_[i] + K2[i]/2)
        K4[i] = h * y_ogolny(y_[i] + K3[i])
        y_[i+1] = y_[i] + ((1/6)*(K1[i] + 2*K2[i] + 2*K3[i] + K4[i]))
    return y_        

y1E, x1E = Eulera(1)
y1K = Kutt(1)

y025E, x025E = Eulera(0.25)
y025K = Kutt(0.25)

y01E, x01E = Eulera(0.1)
y01K = Kutt(0.1)

x1 = np.linspace(0, 10, len(x1E))
x2 = np.linspace(0, 10, len(x025E))
x3 = np.linspace(0, 10, len(x01E))


plt.plot(x1, y1E, label = 'Metoda Eulera h = 1')
plt.plot(x1, y1K, label = 'Metoda Kutta h = 1')
plt.title("Wykres 1")
plt.grid(True)
plt.legend()
plt.show()

plt.figure()
plt.title("Wykres 2")
plt.plot(x2, y025E, label = 'Metoda Eulera h = 0.25')
plt.plot(x2, y025K, label = 'Metoda Kutta h = 0.25')
plt.legend()
plt.grid(True)
plt.show()

plt.figure()
plt.title("Wykres 3")
plt.plot(x3, y01E, label = 'Metoda Eulera h = 0.1')
plt.plot(x3, y01K, label = 'Metoda Kutta h = 0.1')
plt.legend()
plt.grid(True)
plt.show()

y1E = np.array(y1E).reshape((len(y1E)),1)
y1K = np.array(y1K).reshape((len(y1K)),1)
y01E = np.array(y01E).reshape((len(y01E)),1)
y01K = np.array(y01K).reshape((len(y01K)),1)
y025E = np.array(y025E).reshape((len(y025E)),1)
y025K = np.array(y025K).reshape((len(y025K)),1)

wynik1 = np.hstack((y1E, y1K))
wynik2 = np.hstack((y025E, y025K))
wynik3 = np.hstack((y01E, y01K))

pd.set_option('display.max_rows', None)

print("\nTabela wyników dla h = 1" )
tabela1=pd.DataFrame(wynik1, x1E, ['metoda Eulera', 'metoda Rungego-Kutty'])
print (tabela1)

print ("\nTabela wyników dla h = 0.25")
tabela2=pd.DataFrame(wynik2, x025E, ['metoda Eulera', 'metoda Rungego-Kutty'])
print (tabela2)

print ("\nTabela wyników dla h = 0.1")
tabela3=pd.DataFrame(wynik3, x01E, ['metoda Eulera', 'metoda Rungego-Kutty'])
print (tabela3)