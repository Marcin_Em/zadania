# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 13:42:13 2020

@author: Marcin Mokrzycki, 301985
"""
import math
import numpy as np
import datetime

def wpisanie_wartosci_x():
    while True:
        try:
            x1 = float(input("Ile stopni x \n")) 
        except ValueError:
            print("\nWprowadzono niepoprawne dane")
            continue
        else:
            break      
    return x1
def wpisanie_wartosci_y():
    while True:
        try:
            y1 = float(input("Ile stopni y \n")) 
        except ValueError:
            print("\nWprowadzono niepoprawne dane")
            continue
        else:
            break      
    return y1
x1 = wpisanie_wartosci_x()
y1 = wpisanie_wartosci_y()
start = datetime.datetime.now()
def f1(x, y, z):
    return 150*math.cos(x) + 180*math.cos(y) - 200*math.cos(z) - 200

def f2(x, y, z):
    return 150*math.sin(x) + 180*math.sin(y) - 200*math.sin(z) 

def f1primx(x, y, z):
    return -150*math.sin(x)

def f1primy(x, y, z):
    return -180*math.sin(y)

def f2primx(x, y, z):
    return 150*math.cos(x)

def f2primy(x, y, z):
    return 180*math.cos(y)

def Jakobian(x, y, z):
    return [f1primx(x, y, z),f1primy(x, y, z), f2primx(x, y, z), f2primy(x, y, z) ]

def obl():
    for s in range(n):
        for i in range(s+1,a):          ## właściwa petla
            for j in range(s+1,a+1):
                C[i][j] = kekw(i,j,s)
    for i in range(s+1,a):        ## ustawienie 0 w odpowiednich miejscach
        for j in range(s+1):
            C[i][j] = kekw(i,j,s)
    F[n]=C[n][n+1]/C[n][n]
    for i in range(n-1,-1,-1):
        suma=0
        for s in range(i+1,n+1):
            suma=suma + (C[i][s] * F[s])
        F[i]=(C[i,n+1] - suma)/C[i][i]
    return F

def kekw(i,j,s):
    cyfra = (C[i][j])-((((C[i][s])*(C[s][j]))/(C[s][s])))
    return cyfra


U = np.zeros((2,1))
F = np.zeros((2,1))
X = np.zeros((2,1))

itera = 600
E = 0.00001

x = ((x1 * (2*math.pi))/360)
y = ((y1 * (2*math.pi))/360)
z = ((75 * (2*math.pi))/360)

a = 2
n = a-1

X[0][0] = x
X[1][0] = y


k = 0
while k <= itera:

    for i in range(2):
        U[i][0] = X[i][0]
        
    A = Jakobian(X[0][0],X[1][0],z)
    M = np.array(A)
    W = M.reshape(2,2)
    
    F[0] = -(f1(X[0][0],X[1][0],z))
    F[1] = -(f2(X[0][0],X[1][0],z))
    
    C = np.hstack((W,F))
    dX = obl()
    X[0] = X[0] + dX[0]
    X[1] = X[1] + dX[1]

    licz = 0
    for i in range(2):
        if abs(X[i] - U[i]) <= E:
            licz = licz + 1
    if licz == n:
        for i in range (2):
            print (X[i][0])
            break
    
    k = k+1
    
print ("Wynik w radianach: x=%f , y=%f " %(X[0][0], X[1][0]))
print ("Wynik w stopniach: x=%d, y=%d" %((X[0][0] * 360) / (2*math.pi), (X[1][0] * 360) / (2*math.pi)))
duration = datetime.datetime.now() - start
print ("Czas wykonywania programu", duration)