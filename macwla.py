# -*- coding: utf-8 -*-
"""
Created on Sun Jan 17 16:04:26 2021

@author: Marcin Mokrzycki, 301985
"""
import numpy as np   
import math

E = 0.0001      # dokladnosc wyniku

A = np.array(([3 ,-2, 1, 4], [-2, -6, 2, -1], [1, 2, -2, 5], [4, -1, 5, -7]))   # macierz A
W0 = np.identity(4)  # macierz jednostkowa
i = 2               # parametr

while i > 0:        # petla
    if i > 0:
        # szukanie najwikszej wartosci ktora nie jest na przekatnej
        wyn = []    # pusta tablica 
        p1 = []     # pusta tablica
        q1 = []     # pusta tablica
        
        for i in range(4):      # petla for
            for j in range(4):  
                if i != j:      # kiedy i oraz j sa rozne
                    wyn.append(abs(A[i][j]))    # wartosc jest przypisywana do pustej tablicy
                    p1.append(i)                # wartosc jest przypisywana do pustej tablicy
                    q1.append(j)                # wartosc jest przypisywana do pustej tablicy
        
        m = wyn.index(max(wyn))     # pozycja najwiekszej wpisanej wartosci
        p = p1[m]                   # przypisanie odpowiedniego p
        q = q1[m]                    # przypisanie odpowiedniego q
        
        n = (A[q][q] - A[p][p]) / (2 * A[q][p]) # obliczanie wartosc n
                    # trzy petle for sa odpowiedzialne za zamiane sgn(n)
        if n < 0:
            nn = -1
        if n > 0:
            nn = 1
        if n == 0:
            nn = 0
        t = nn / (abs(n) + math.sqrt(n**2 + 1))     # obliczanie wartosci nn
        c = 1 / math.sqrt(t**2 + 1)                 # obliczanie wartosci c
        s = t * c                                   # obliczanie wartosci s
        Q = np.identity(4)              # macierz jednostkowa 
        Q1 = np.identity(4)             # macierz jednostkowa pomocnicza
        for r in range(4):  
            Q[r][p] = c*Q1[r][p] - s*Q1[r][q]       # zamiana wartosci w Q zgodnie ze wzorem
            Q[r][q] = s*Q1[r][p] + c*Q1[r][q]       # zamiana wartosci w Q zgodnie ze wzorem
        
        A1 = np.transpose(Q) @ A @ Q                # mnozenie macierzowe 

        W = W0 @ Q      # mnozenie macierzowe
    
        wero = []       # puste tablice pomocnicze
        weri = [] 
        for i in range(4):
            for j in range(4):
                if i != j :
                    wero.append(abs(A[i][j]))   # do tablicy sa wpisywane wartosci poza przekatnymi
                if i == j :
                    weri.append(abs(A[i][j]))   # do tablicy sa wpisywane wartosci z przekatnych
                    
        
        dokl =  (max(wero) / max(weri))         # sprawdzenie dokladnosci, podzielenie wartosci max poza przekatnymi / max z przekatnych
        if dokl < 0.0001:               # jesli dokladnosc jest zadowalajaca petla jest przewrana
            break
        
        A = A1      # macierz z wartosciami wlasnymi / macierz wykorzystywana w kolejnych iteracjach 
        W0 = W      # macierz z wektorami wlasnymi / macierz wykorzystywana w kolejnych iteracjach
        

wartosci = []       # pusta tablica
for i in range(4):
    for j in range(4):
        if i == j :     # podrzebne sa tylko wartosci na przekatnej , i = j
            wartosci.append(A[i][j])    # zapis odpowiednich wartosci do tablicy

print ("Macierz wektorow: \n",W)        # zapis odpowiedzi
print ("Wartosci wlasne: \n", wartosci)     # zapis odpowiedzi
